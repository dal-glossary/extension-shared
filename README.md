Shared Extension Components
=================

This repository contains code that is used for both the [Firefox](https://bitbucket.org/dal-glossary/firefox-add-on) and [Chrome](https://bitbucket.org/dal-glossary/chrome-extension) extensions that are part of the Dalhousie Glossary system.  It is comprised of two components:

  - highlighting
  - term viewer

__Note__ Currently, all data is loaded from http://localhost:8080.  This is temporary.


####Highlighting
 The highlighting component is a javascript library that can scan a page of HTML and wrap words from a list in span tags.  These tags are styled and made clickable.

####Term Viewer
 The Term Viewer is used to display information about a term.  The information is read from a server and displayed to the user.  For each term, the server provides some number of entries about that term, which the Term Viewer displays.  For more information about the REST format, see the [server wiki](https://bitbucket.org/dal-glossary/server/wiki/Home)

### Dependencies
This project uses the [Grunt](http://gruntjs.com/) build tool, which is based on [Node.js](http://nodejs.org/).

The Highlighter depends on [promise.js](https://github.com/stackp/promisejs), and Oleg Masko's javscript [snowball stemmer](http://code.google.com/p/urim/).

The Term Viewer depends on [EmberJS](http://emberjs.com/), [Handlebars](http://handlebarsjs.com/) and [jQuery](http://jquery.com/)

All dependencies are included in the repository.

###Usage

####Highlighter

 To use the highlighter in a project:

  1. Run `npm install` to configure the grunt dependencies
  2. Run `grunt` to build the project.
  3. Include `src/highlighting/build/highlight.lib.js` and `src/highlighting/build/highlight.js` in your project (in that order)
  4. Provide the required functions (see below for their semantics):
    - `requestStyleSheetUpdate()`
    - `performGetFor(path)`
    - `displayTerm(term)`
  5. Invoke `addHighlighting()` with no arguments on the page you wish to highlight

 This will cause all words on the page that match a list to be wrapped in spans and styled.  The required functions dictate where the list is loaded from, what styling is applied and what happens when a word is clicked on.

 `requestStyleSheetUpdate()` should look up the current styling preferences and pass them to the function `updateStyleSheet(style)` where `style` is a javascript object with the following keys:

  - `bold`: either `bold` or `normal` (will be assigned to the `font-weight` css attribute)
  - `underline`: either `underline` or `none` (will be assigned to the `text-decoration` css attribute)
  - `color`: an HTML color code (will be assigned to the `color` css attribute)
  - `colorenabled`: true if highlighted spans should have the text color changed, false otherwise.

  This object will determine the styling of any terms that are matched in the HTML document.

 `performGetFor(path)` returns a promise that completes after an HTTP request returns.  This function should look up the currently configured server url and append the `path` parameter, then use the concatenated url to do a get request.  The result of that get request is the promise that is returned.  The callback format should be `function(error, result)`

 `displayTerm(term)` should perform some action to display information about the parameter `term`

#####Testing
 The highlighter has a number of acceptance tests written for it, using the [Jasmine](http://pivotal.github.io/jasmine/) framework.  To invoke these tests, run `grunt test`.  This will run the tests using [PhantomJS](http://phantomjs.org/).

 To run the tests in a browser, execute the test ask as above, and then open the file SpecRunner.html, which has been generated in the build directory.

####TermViewer

To use the term viewer, you must first build the project using grunt.

 1. Run `npm install` to configure the grunt dependencies
 2. Run `grunt` to build the project.
 3. Open the file `build/termview.html` in a browser.
 4. Add a fragment of the form `#/term/<term>` to the end of the url, where `<term>` is the word you wish to look up.

This assumes you have a functioning webserver at `http://localhost:8080` conforming to the API.

#####Testing
There is currently no automated testing of TermViewer.

###Git Notes

The Firefox and Chrome extensions share this code as a [git submodule](http://git-scm.com/book/en/Git-Tools-Submodules).

Briefly, this means that each of the Firefox and Chrome extension repositories are dependant on this one, although this is not dependant on them.  This repo can be updated and cloned without working through either of the other extensions.

###Structure

The `src` folder contains the source code for each of the components.  The highlighting component is in `src/highlighting` and the term viewing component is in `src/termview.`  Each component contains some of the following directories:

 - `lib` where any the source and dependencies of the component are stored.
 - `lang` contains translation files for strings.

 The `test` folder contains the automated testing, broken down by component

 The `build` folder will be created by the build task of the grunt file.

The following grunt targets are available in the root project:

 - `build`: Combine and minify each of the subprojects after compiling from CoffeeScript
 - `clean`: obliterates the `build` directory.
 - `test`: Runs all jasmine tests.

 In addition, the grunt build supports the following options (for `build` and `test`):

  - `--dev`: maintain the source map files that can be used for debugging.  This will work on newer versions of Chrome and Firefox from version 23 on.  Firebug does not support it.
  - `--no-lib`: Pass in if you wish to supress compilation of the libraries.  They take longer than anything else, and if they've already been built, there's no reason to build them again.

