window.popin = (element) ->
    element.classList.add('popin');
    ref = null
    close = () ->
        element.style.display = '';
        ref.removeEventListener('click', close, true)
    return {
        open: (reference) ->
            ref = reference
            element.style.display = 'inline';
            reference.parentNode.insertBefore(element, reference.nextSibling);
            reference.addEventListener('click',  close, true)
            reference.classList.add('popinReference')
        close: ->
            close();
    }