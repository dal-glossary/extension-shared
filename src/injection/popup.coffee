###
    popup() is a function for displaying a popup in relation to a reference element.  The popup will be displayed above
     and slightly off-centre of the reference element, with a connecting piece drawn between then to show their
    connection.

    This module exposes a function popup(), which can be applied to a DOM element.  This DOM element will be hidden.
    The value returned from the popup function is an object with two methods: open() and close().  open() will display
    the element that was passed to popup, either in the centre of the page, or optionally close to a reference object.
    close() will close the display popup.  Clicking anywhere but the popup, or pressing escape will also hide the popup.
    The CSS stylesheet `popup.css` should also be included.

    Example (assumes you have an HTML element with ID 'popup' that you want to display near another HTML element with ID
    'reference':

        //Creates a popup
        var myPopup = popup(document.getElementById('popup'));

        //Finds the reference element
        var reference = document.getElementById('reference');

        //When the reference element is clicked...
        reference.addEventListener("click", function() {
            //Display the popup close to the reference element
            myPopup.show(reference);
        });

    `popup(element, options)` : Creates a popup out of `element` with options given by `options`.  Immediately hides the
                                element.  The return value can be used to open or close the popup (it is closed by
                                default)

       - *element* a DOMElement that will be displayed when the popup is shown.  This element is hidden and will only be
          displayed when `open` is called on the return value from this function
       - *options* (optional) An object with the following (optional) attributes:
            - _hoffset_ (default 20) the horizontal offset in pixels from the "default" location of the popup
            - _voffset_ (default 20) the vertical offset in pixels from the "default" location of the popup
            - _padding_ (default 25) the distance from the edge of the viewport in pixels that the popup is guaranteed
                to be, regardless of the position of the reference element and the hoffset and voffset parameters
            - _connectorColor_ (default {red: 192, green: 192, blue: 192}) an object with three properties (red, green
                and blue) specifying the color of the connector between the reference block and the popup

    `open(reference)`: called on the value returned from `popup()` above.  This will cause the popup to be displayed,
                       centred and above the value passed in to reference, or centred on the screen if no element is
                       passed in.  The precise relationhip between the location is determined by a number of factors,
                       including the hoffset and voffset parameters passed in through `options` to `popup()` and the
                       location of the `reference` element. Essentially, the popup will always be onscreen, and will
                       always be more than `options.padding` pixels from the edge of the screen.  Furthermore, this
                       position will be updated so as to maintain the constraints, even if the elements change
                       size or location
        - *reference* (optional) the element that this popup will be connected to.

    `close()`:  closes the popup box if it is open.

###
window.popup = (element, options) ->

    # This will cause the popup element to disappear and otherwise be formatted correctly.  This is done with a
    # stylesheet instead of directly on the element to allow the default behavior to be overridden if necessary
    element.classList.add('popup');

    #Parse the options object and fill in any missing values
    options ?= {}
    options.hoffset ?= 20;
    options.voffset ?= 20;
    options.padding ?= 25;
    options.connectorColor ?= {
        red: 192
        blue: 192
        green: 192
    }

    #Create the connecting box, and make sure it is hidden
    connectingBox = document.createElement('div')
    connectingBox.style.display = 'none'
    connectingBox.style.position = 'absolute'

    document.body.appendChild(connectingBox);

    #Close does nothing if there is no window open
    close: ->

    #The open function (see the documentation at the beginning of the file)
    open: (reference) ->

        #The updatePosition() function is called when the popup is first opened and also whenever anything happens
        #that might cause the position or size of the popup element to change.
        updatePosition = () ->

            #Make sure of the scope of the above variable
            above = true

            #figure out how large the viewport is
            viewportRect = {
                width: document.documentElement.clientWidth
                height: document.documentElement.clientHeight
            }


            #get the bounding rect of the popup box (which at this point must be visible, or we will get some very wrong
            #values
            popupRect = element.getBoundingClientRect();

            #Set the position of the popup
            position = if reference?

                #Having the reference element wrap across lines causes some WEIRD behavior that I'd like to avoid
                reference.style.whiteSpace = 'nowrap';

                #get the bounding rectangle for the reference object
                referenceRect = reference.getBoundingClientRect();

                #sort out where the centre of the reference rect is horizontally (we'll need this to align the two
                #rectangles
                refCentre = (referenceRect.right + referenceRect.left) / 2
                #Find half the width of the popup rect, which is also needed to align them
                halfWidth = (popupRect.right - popupRect.left) / 2

                #This is the return value: an object with a left and top property.
                {
                    #if the popup would be off the right side of the screen
                left: if refCentre + options.hoffset + halfWidth > viewportRect.width - options.padding

                    #check if inverting the offset would put it off the left side
                    if(refCentre - options.hoffset - halfWidth < options.padding)
                        #make the left the miniumum possible
                        options.padding + window.scrollX
                    else
                        #Invert the offset, which should make it further left
                        refCentre - options.hoffset - halfWidth + window.scrollX
                else
                    #if the popup would be off the left sie of the screen
                    if(refCentre + options.hoffset - halfWidth < options.padding)
                        #make the left the minimum possible
                        options.padding + window.scrollX
                    else
                        #Put it where requested
                        refCentre + options.hoffset - halfWidth + window.scrollX


                #If the popup would be off the top of the screen
                top: if referenceRect.top - popupRect.height - options.voffset < 0

                    #If putting it below the reference element would put it off the bottom of the screen
                    if(referenceRect.bottom + popupRect.height + options.voffset > viewportRect.height - options.padding)
                        #Put it at the lowest possible value
                        above = true
                        options.padding + window.scrollY
                    else
                        # Put it voffset below the reference element
                        above = false
                        referenceRect.bottom + options.voffset + window.scrollY
                else
                    #Put it above the element
                    above = true
                    referenceRect.top - popupRect.height - options.voffset + window.scrollY
                }
            else
                #If we don't have a reference element, just centre it and be done with it
                {
                left: (viewportRect.width - popupRect.width) / 2
                top: (viewportRect.height - popupRect.height) / 2
                }

            #Place the popup
            element.style.top = position.top + "px";
            element.style.left = position.left + "px";


            #Set up the connecting box
            if(reference)

                #make the connecting box visible
                connectingBox.style.display = 'block'

                #Figure out the dimensions of the popup box
                myWidth = popupRect.right - popupRect.left;
                myHeight = popupRect.bottom - popupRect.top;

                #We will only need the width of the refernce element
                refWidth = referenceRect.right - referenceRect.left

                #Make sure heightGap is scoped correctly.
                heightGap = 0


                legWidth = Math.abs((myWidth - refWidth) / 2)

                #Where the popup would be if it were centred around the reference element
                idealLeft = referenceRect.left - (myWidth - refWidth) / 2

                #The color to be used for the connecting box as a CSS string
                borderColor = "rgba(#{options.connectorColor.red},#{options.connectorColor.green},#{options.connectorColor.blue},0.5)"

                if above
                    heightGap = referenceRect.top - position.top - myHeight + window.scrollY
                    if myWidth > refWidth
                        connectingBox.style.width = refWidth + 'px'
                        connectingBox.style.borderTop = "#{heightGap}px solid #{borderColor}"
                        connectingBox.style.borderBottom = ""
                    else
                        connectingBox.style.width = myWidth + 'px'
                        connectingBox.style.borderBottom = "#{heightGap}px solid #{borderColor}"
                        connectingBox.style.borderTop = ""
                    connectingBox.style.top = "#{position.top + myHeight}px"

                else
                    heightGap = position.top - referenceRect.bottom - window.scrollY
                    if myWidth > refWidth
                        connectingBox.style.width = refWidth + 'px'
                        connectingBox.style.borderBottom = "#{heightGap}px solid #{borderColor}"
                        connectingBox.style.borderTop = ""
                    else
                        connectingBox.style.width = myWidth + 'px'
                        connectingBox.style.borderTop = "#{heightGap}px solid #{borderColor}"
                        connectingBox.style.borderBottom = ""
                    connectingBox.style.top = "#{referenceRect.bottom + window.scrollY}px"

                connectingBox.style.borderLeft = "#{legWidth}px solid transparent"
                connectingBox.style.borderRight = "#{legWidth}px solid transparent"
                skewAngle = Math.atan2(heightGap, position.left - idealLeft) * 180 / Math.PI - 90;
                skewAngle = -skewAngle unless above
                connectingBox.style.webkitTransform = "skewx(#{skewAngle}deg)";
                connectingBox.style.MozTransform = "skewx(#{skewAngle}deg)";
                connectingBox.style.oTransform = "skewx(#{skewAngle}deg)";
                connectingBox.style.Transform = "skewx(#{skewAngle}deg)";

                connectingBox.style.left = if myWidth > refWidth then "#{position.left + (idealLeft - position.left)/2}px" else "#{referenceRect.left + (idealLeft - position.left)/2}px"


        #Don't attach event listeners if the popup is already displayed
        if(element.style.display isnt 'block')
            config = {
                attributes: true
                characterData: true
                subTree: true
            }

            changeHappened = () ->
                observer.disconnect();
                updatePosition()
                observer.observe(element, config);
                if reference
                    observer.observe(reference, config)
            observer = new MutationObserver? (mutations) ->
                changeHappened()


            observer?.observe(element, config);
            if reference
                observer?.observe(reference, config)

            window.addEventListener('resize', changeHappened, true)
            document.addEventListener('scroll', changeHappened, true)

            #only respond to clicks on elements that are not the popup
            clickHandler = (event) =>
                cElem = event.target
                while cElem
                    if cElem is element
                        return
                    cElem = cElem.parentNode
                @close()

            keyHandler = (event) =>
                if event.keyCode is 27
                    @close()

            document.addEventListener "click", clickHandler, true
            document.addEventListener "keypress", keyHandler, true


            @close = () ->
                observer?.disconnect()
                element.style.display = 'none'
                connectingBox.style.display = 'none'
                document.removeEventListener "click", clickHandler, true
                document.removeEventListener "keypress", keyHandler, true
                reference?.classList.remove('popupReference')
                @close = ->


        element.style.display = 'block';
        element.style.position = 'absolute';
        reference?.classList.add('popupReference')
        updatePosition()


