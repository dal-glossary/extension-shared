
###
Handles all highlighting for the glossary system.



The highlighting mechanism scans a webpage for words included on its list and surrounds any found words with a span.

The list of words is pulled from an REST server at the path '/words'.  This should return a JSON array of strings,
where each string is a word to be highlighted.

Each term is changed to lowercase and stemmed.  If the term is made up of multiple words, then each word is stemmed
independently.  The stemmed words are then stored in a trie.

The function then descends through the DOM searching for elements with textual content which are inside of a 'main
content element' (within a p element, or divs with certain classes).  Any text nodes are processed with a finite
state machine which recognizes words.  The machine will stop at the end of each word it finds, stem the word, and
attempt to match it to something in the trie.  If the word is possibly part of a longer match, then it continues
matching words until they no longer match anything the trie.  The longest valid match is used.  A matched word (or
string of words) are pulled out of their text element, wrapped in a span, and then re-inserted into the DOM.

The end result is that any string of words which matches any term in the list after stemming are highlighted.
###
window.addHighlighting = ->
  "use strict"
  words = {} #The trie that will store terms in the list.
  currentResults = {} # Stores the various variables that matchesWord() uses for processing
  styleElement = undefined
  # Created to hold the styling that will be used to highlight matched terms
  snowball = new Snowball("English") #The snowball stemmer
  ###
  Creates the style element that will style the highlighted words on the page.
  ###
  addStyleElement = ->
    styleElement = document.createElement("style")
    styleElement.setAttribute "type", "text/css"
    document.head.appendChild styleElement


  ###
  Performs the matching.

  This function contains the heart of the matching logic.  It decends through the DOM and matches each word
  that it finds.  The finite state machine to find words is in parseText, and the matching mechanism is
  within matchedWord.
  ###
  onComplete = ->

    ###
    Checks the currently  matched words against the trie.  Verifies that we currently have a match and also
    if there could be another match with more letters added.

    @param afterText
    @param currentWord
    @return {Boolean}
    ###
    matchesWord = (afterText, currentWord) ->
      word = currentWord.toLowerCase()
      charIdx = undefined
      currentNode = currentResults.matchedNode
      curChar = undefined
      runningMatch = undefined
      runningParse = undefined
      nodeEntry = undefined
      currentResults.parsedWholeWord = false
      snowball.setCurrent word
      snowball.stem()
      word = snowball.getCurrent()
      charIdx = 0
      while charIdx < word.length
        curChar = word[charIdx]
        return false  if currentNode[curChar] is `undefined`
        currentNode = currentNode[curChar]
        charIdx += 1
      currentResults.parsed += word
      runningMatch = currentResults.matched + currentWord
      runningParse = currentResults.parsed
      if currentNode.end
        currentResults.longestMatch = runningMatch
        currentResults.longestParsed = runningParse
      charIdx = 0
      while charIdx < afterText.length
        curChar = afterText[charIdx]
        runningMatch += curChar
        runningParse += curChar
        return false  if currentNode[curChar] is `undefined`
        currentNode = currentNode[curChar]
        if currentNode.end
          currentResults.longestMatch = runningMatch
          currentResults.longestParsed = runningParse
        charIdx += 1
      currentResults.matched = runningMatch
      currentResults.parsed = runningParse
      currentResults.matchedNode = currentNode
      currentResults.parsedWholeWord = true

      #This loops through each property in the current node to check if there are
      #any more options for the trie.  If there are, then it will bail out and return true
      #If the only option is end or there are no options, then the matching must stop
      #and we return false
      for nodeEntry of currentNode
        return true  if currentNode.hasOwnProperty(nodeEntry) and nodeEntry isnt "end"
      false

    #Steps for parsing words
    #-Store any initial non-word characters in a 'before' string
    #-work through the word until a non-word character is found
    #-stick any non-word characters in an 'after' string
    #-stem the word
    #-match the word, and then each character in the after string until the longest matching substring is found
    #-If the longest matching string is less than the word and 'after' or no longer matching string exists, then
    #  -We found the word!
    #-Otherwise
    #  - back to step 2
    # After completion go back to the beginning of the first non-matched word, or the second word, whichever comes second
    parseText = (text, node, parent) ->
      textIdx = undefined
      beforeText = ""
      afterText = ""
      currentWord = ""
      currentText = ""
      initialWord = undefined
      curChar = undefined
      followingChar = undefined
      initialAfter = undefined
      wordStart = undefined
      spaceIndex = undefined
      newSpan = undefined
      referenceNode = node
      wordRegex = /\w/
      PARSE_STATE =
        BEFORE: 1
        WORD_FIRST: 2
        AFTER_FIRST: 3
        WORD_LATER: 4
        AFTER_LATER: 5

      state = PARSE_STATE.BEFORE
      resetResults = ->
        currentResults.parsed = ""
        currentResults.matched = ""
        currentResults.matchedNode = words
        currentResults.longestMatch = ""
        currentResults.longestParsed = ""

      resetResults()
      textIdx = 0
      while textIdx < text.length
        curChar = text[textIdx]
        switch state
          when PARSE_STATE.BEFORE
            if wordRegex.test(curChar)
              currentWord = curChar
              state = PARSE_STATE.WORD_FIRST
              wordStart = textIdx
            else
              beforeText += curChar
          when PARSE_STATE.WORD_FIRST
            if wordRegex.test(curChar)
              currentWord += curChar
            else
              afterText = curChar
              state = PARSE_STATE.AFTER_FIRST
          when PARSE_STATE.AFTER_FIRST
            if wordRegex.test(curChar)

              #If the text is only an apostrophe, then this hasn't been a break in the word
              if afterText is "'"
                currentWord += afterText + curChar
                afterText = ""
                state = PARSE_STATE.WORD_FIRST
              else if matchesWord(afterText, currentWord)
                initialWord = currentWord
                followingChar = curChar
                initialAfter = afterText
                currentWord = curChar
                spaceIndex = textIdx
                afterText = ""
                state = PARSE_STATE.WORD_LATER
              else
                if currentResults.longestMatch.length > 0
                  currentText += beforeText
                  parent.insertBefore document.createTextNode(currentText), node  if currentText.length > 0
                  newSpan = document.createElement("span")
                  newSpan.setAttribute "class", "glossary_term"
                  newSpan.appendChild document.createTextNode(currentResults.longestMatch)
                  parent.insertBefore newSpan, node
                  referenceNode = newSpan
                  beforeText = afterText.substr(currentResults.longestMatch.length - currentWord.length)
                  currentWord = curChar
                  resetResults()
                  currentText = ""
                  state = PARSE_STATE.WORD_FIRST
                  wordStart = textIdx
                  followingChar = curChar
                else
                  currentText += beforeText + currentWord
                  beforeText = afterText
                  state = PARSE_STATE.WORD_FIRST
                  wordStart = textIdx
                  currentWord = curChar
                  resetResults()
            else
              afterText += curChar
          when PARSE_STATE.WORD_LATER
            if wordRegex.test(curChar)
              currentWord += curChar
            else
              afterText = curChar
              state = PARSE_STATE.AFTER_LATER
          when PARSE_STATE.AFTER_LATER
            if wordRegex.test(curChar)

              #If the text is only an apostrophe, then this hasn't been a break in the word
              if afterText is "'"
                currentWord += afterText + curChar
                afterText = ""
                state = PARSE_STATE.WORD_LATER
              else if matchesWord(afterText, currentWord)
                currentWord = curChar
                state = PARSE_STATE.WORD_LATER
                afterText = ""
              else
                if currentResults.longestMatch.length > 0
                  currentText += beforeText
                  parent.insertBefore document.createTextNode(currentText), node  if currentText.length > 0
                  newSpan = document.createElement("span")
                  newSpan.setAttribute "class", "glossary_term"
                  newSpan.appendChild document.createTextNode(currentResults.longestMatch)
                  parent.insertBefore newSpan, node
                  referenceNode = newSpan
                  beforeText = "" #afterText.substr(currentResults.longestMatch.length - wordSoFar.length - currentWord.length);
                  currentWord = ""
                  textIdx = wordStart + currentResults.longestMatch.length - 1
                  resetResults()
                  currentText = ""
                  state = PARSE_STATE.BEFORE
                else
                  currentText += beforeText + initialWord
                  beforeText = initialAfter
                  state = PARSE_STATE.WORD_FIRST
                  textIdx = spaceIndex
                  currentWord = followingChar
                  resetResults()
            else
              afterText += curChar
        textIdx += 1
      currentText += beforeText
      if currentText.length > 0
        referenceNode = document.createTextNode(currentText)
        parent.insertBefore referenceNode, node
      if currentWord.length > 0
        matchesWord afterText, currentWord
        if currentResults.longestMatch.length > 0
          newSpan = document.createElement("span")
          newSpan.setAttribute "class", "glossary_term"
          newSpan.appendChild document.createTextNode(currentResults.longestMatch)
          parent.insertBefore newSpan, node
          referenceNode = newSpan
        if currentResults.parsedWholeWord
          afterText = currentResults.matched.substr(currentResults.longestMatch.length)
        else
          afterText = (currentResults.matched + currentWord + afterText).substr(currentResults.longestMatch.length)
      if afterText.length > 0
        referenceNode = document.createTextNode(afterText)
        parent.insertBefore referenceNode, node
      referenceNode
    isParseableNode = (node) ->
      return true  if node.tagName is "P" or /.*[cC]ontent.*/.test(node.id)
      false
    result = []
    root = document.body
    node = root.firstChild
    parent = root
    withinParseable = false
    parsedNode = undefined
    foundOne = undefined
    while node isnt null
      foundOne = false
      withinParseable = node  if not withinParseable and isParseableNode(node)
      if node.nodeType is Node.TEXT_NODE and node.parentNode.tagName.toLowerCase() isnt "script"
        if withinParseable
          parsedNode = parseText(node.textContent, node, parent)
          if parsedNode isnt node
            parent.removeChild node
            node = parsedNode
            foundOne = true
      if node.hasChildNodes() and not foundOne
        parent = node
        node = node.firstChild
      else
        while node.nextSibling is null and node isnt root
          parent = parent.parentNode
          node = node.parentNode
          withinParseable = false  if node is withinParseable
        node = node.nextSibling
    root.className += " highlighted_text"
    addStyleElement()
    requestStylesheetUpdate()
    glossary_terms = document.getElementsByClassName("glossary_term")
    click_function = ->
      text = @textContent
      snowball.setCurrent text.toLowerCase()
      snowball.stem();
      currentNode = words;
      text = snowball.getCurrent();
      for letter in text
        currentNode = currentNode[letter]

      if(currentNode.termID)
        displayTerm currentNode.termID, this



    for curSpan of glossary_terms
      glossary_terms[curSpan].onclick = click_function  if glossary_terms.hasOwnProperty(curSpan)
    result

  window.updateStyleSheet = (style) ->
    styleElement.innerHTML = ".glossary_term{text-decoration:" + style.underline + ";font-weight:" + style.bold + ((if style.colorenabled then (";color:" + style.color) else "")) + ";}"

  performGetFor("word").then (error, result) ->
    termList = undefined
    termIdx = undefined
    charIdx = undefined
    currentNode = undefined
    term = undefined
    curChar = undefined
    inWord = true
    currentWord = undefined
    stemmedTerm = undefined
    isWordChar = undefined
    if not error and result

      #Get the list of words as an array
      termList = JSON.parse(result)

      #Store the word list into a trie for fast, incremental retrieval
      termIdx = 0
      while termIdx < termList.length
        currentNode = words
        term = termList[termIdx].name
        termID = termList[termIdx].id
        currentWord = ""
        stemmedTerm = ""
        charIdx = 0
        while charIdx < term.length
          curChar = term[charIdx].toLocaleLowerCase()
          isWordChar = /\w/.test(curChar)
          if inWord
            if isWordChar
              currentWord += curChar
            else
              snowball.setCurrent currentWord
              snowball.stem()
              stemmedTerm += snowball.getCurrent() + curChar
              currentWord = ""
              inWord = false
          else
            if isWordChar
              currentWord = curChar
              inWord = true
            else
              stemmedTerm += curChar
          charIdx += 1
        if currentWord.length > 0
          snowball.setCurrent currentWord
          snowball.stem()
          stemmedTerm += snowball.getCurrent()
        charIdx = 0
        while charIdx < stemmedTerm.length
          curChar = stemmedTerm[charIdx]
          currentNode[curChar] = {}  if currentNode[curChar] is `undefined`
          currentNode = currentNode[curChar]
          charIdx += 1
        currentNode.end = true
        currentNode.termID = termID
        termIdx += 1
      onComplete()