window.TermView = Ember.Application.create()
TermView.Router.map ->
  "use strict"
  @resource "termview",
    path: "/term/:termid"


TermView.IndexRoute = Ember.Route.extend(redirect: ->
  "use strict"
  @transitionTo "termview", "html"
)
TermView.TermviewRoute = Ember.Route.extend(model: (params) ->
  "use strict"
  TermView.Term.find params.termid
)
TermView.Term = DS.Model.extend(
  text: DS.attr("string")
  entries: DS.hasMany("TermView.Entry")
)
TermView.Entry = DS.Model.extend(
  term: DS.belongsTo("TermView.Term")
  text: DS.attr("string")
  type: DS.attr("string")
)
TermView.TermviewController = Ember.ObjectController.extend(headings: ->
  "use strict"
  entries = @get("model.entries")
  headingHash = {}
  headings = []
  e = undefined
  entries.forEach (entry, index) ->
    type = entry.get("type")
    if headingHash[type] is `undefined`
      headingHash[type] =
        title: _(type)
        entries: []
    headingHash[type].entries.push entry

  for e of headingHash
    headings.push headingHash[e]  if headingHash.hasOwnProperty(e)
  headings
  .property("model.entries.@each.type", "model.entries.@each.text"))
TermView.Store = DS.Store.extend(adapter: "DS.RESTAdapter")
DS.RESTAdapter.reopen url: "http://127.0.0.1:8080"
DS.RESTAdapter.configure "plurals",
  entry: "entries"
