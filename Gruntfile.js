module.exports = function(grunt) {
    'use strict';


    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        uglify: {
            libraries: {
                options: {
                    sourceMap: grunt.option('dev') ? function(uglified) {
                        return uglified + '.map';
                    } : undefined,
                    //sourceMapRoot: '../src/highlighting',
                    sourceMappingURL: function(map) {
                        return (/build\/(.+)/).exec(map)[1] + '.map';
                    }
                },
                files: grunt.option('no-lib') ? [] : [{
                    'build/highlight.lib.js': 'src/highlighting/lib/*.js',
                    'build/termview.lang.js': 'src/termview/lang/en.js',
                    'build/termview.lib.js': 'src/termview/lib/*.js'
                }]
            },

            highlighting: {
                options: {


                    sourceMap: grunt.option('dev') ? 'build/highlight.min.js.map' : undefined,
                    sourceMapRoot: '../src/highlighting',
                    sourceMapIn: 'build/highlight.coffee.js.map'
                },
                files: [ {
                    'build/highlight.min.js': 'build/highlight.coffee.js'
                } ]
            },
            termview: {
                options: {
                    sourceMap: grunt.option('dev') ? 'build/termview.min.js.map' : undefined,
                    sourceMapRoot: '../src/termview',
                    sourceMapIn: 'build/termview.coffee.js.map'
                },
                files: [ {
                    'build/termview.min.js': 'build/termview.coffee.js'
                } ]
            },
            injection: {
                options: {
                    sourceMap: grunt.option('dev') ? 'build/injection.min.js.map' : undefined,
                    sourceMapRoot: '../src/injection',
                    sourceMapIn: 'build/injection.coffee.js.map'
                },
                files: [ {
                    'build/injection.min.js': 'build/injection.coffee.js'
                } ]
            }

        },
        copy: {
            all: {
                files: [
                    {
                        expand: true,
                        src: ['src/termview/*.html', 'src/termview/*.css', 'src/welcome/*', 'src/injection/*.css'],
                        dest: 'build/',
                        flatten: true
                    }
                ]
            }
        },

        jasmine: {
            highlighting: {
                src: ['build/highlight.lib.js', 'build/injection.min.js', 'build/highlight.min.js'],
                options: {
                    outfile: 'build/SpecRunner.html',
                    vendor: 'test/highlighting/lib/*.js',
                    specs: 'test/highlighting/spec/*Spec.js',
                    helpers: ['test/highlighting/spec/*Helper.js'],
                    template: 'test/highlighting/highlight.tmpl',
                    styles: ['build/popup.css'],
                    keepRunner: true
                }
            }
        },

        coffee: {
            dev : {
                options: {
                    sourceMap: grunt.option('dev') ? true: undefined
                },
                files: [{
                    'build/highlight.coffee.js': 'src/highlighting/*.coffee',
                    'build/termview.coffee.js': 'src/termview/*.coffee',
                    'build/injection.coffee.js': 'src/injection/*.coffee'
                }]
            }

        },
        clean:  ['build'],

        fixSourceMap: {
            files: ['build/highlight.min.js',
                'build/termview.min.js', 'build/injection.min.js'].concat(grunt.option('no-lib')? [] : [
                'build/termview.lib.js',
                'build/termview.lang.js',
                'build/highlight.lib.js'])
        }




    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-coffee');

    grunt.registerTask('fixSourceMap', "Fixes the source map file structure that uglify can't handle", function() {
        if(grunt.option('dev')) {
            var data = grunt.config('fixSourceMap');
            var whatever = grunt.config('uglify');
            data.files.forEach(function(file) {
                var sourceCode = grunt.file.read(file),
                    sourceMap = grunt.file.readJSON(file + ".map"),
                    file_parts = sourceMap.file.split(/\/+|\\+/),
                    newSources = [];

                //Strip the build path out of the source map reference in the minified file
                grunt.file.write(file,
                    sourceCode.replace(/\/\*\s*\/\/@\s*sourceMappingURL=build\//, "/*\n//@ sourceMappingURL="));
                grunt.log.writeln("Updated source file '" + file + "'");

                //Remove the directory components from the file attribute
                sourceMap.file = file_parts.pop();
                sourceMap.sources.forEach(function(mapFile) {
                    //Remove the directory components from each of the source filenames
                    file_parts = mapFile.split(/\/|\\/);
                    newSources.push(file_parts.pop());
                });
                sourceMap.sources = newSources;

                //Use the directory component of the most recent source file as the new source root.
                //This assumes that all the sources come from the same directory.
                sourceMap.sourceRoot = file_parts.join('/');

                grunt.file.write(file + ".map", JSON.stringify(sourceMap));
                grunt.log.writeln("Updated map file '" + file + ".map'");
            });
        }
    });

    grunt.registerTask('build', ['copy:all', 'coffee', 'uglify', 'fixSourceMap']);


    grunt.registerTask('test', ['build', 'jasmine']);

    // Default task(s).
    grunt.registerTask('default', ['build']);


};