var commonWords = ["Mrs. Dalloway", "Clarissa Dallowayy", "Of Stuff", "Very upright", "brother-in-law", "For Lucy", "herself.", "a", "able", "about", "above", "across", "act", "action", "actually", "add", "addition", "adjective", "afraid", "Africa", "after", "again", "against", "age", "ago", "agreed", "ahead", "air", "all", "allow", "almost", "alone", "along", "already", "also", "although", "always", "am", "America", "among", "amount", "an", "and", "angle", "animal", "another", "answer", "any", "anything", "appear", "apple", "are", "area", "arms", "army", "around", "arrived", "art", "as", "ask", "at", "away","baby", "back", "bad", "ball", "bank", "base", "be", "bear", "beat", "beautiful", "became", "because", "become", "bed", "been", "before", "began", "begin", "behind", "being", "believe", "bell", "belong", "below", "beside", "best", "better", "between", "big", "bill", "birds", "bit", "black", "block", "blood", "blow", "blue", "board", "boat", "body", "bones", "book", "born", "both", "bottom", "box", "boy", "branches", "break", "bright", "bring", "British", "broken", "brother", "brought", "brown", "build", "building", "built", "burning", "business", "but", "buy", "by","call", "came", "can", "cannot", "can't", "capital", "captain", "car", "care", "carefully", "carry", "case", "cat", "catch", "cattle", "caught", "cause", "cells", "center", "cents", "century", "certain", "chance", "change", "chart", "check", "chief", "child", "children", "choose", "church", "circle", "city", "class", "clean", "clear", "climbed", "close", "clothes", "cloud", "coast", "cold", "color", "column", "come", "common", "company", "compare", "complete", "compound", "conditions", "consider", "consonant", "contain", "continued", "control", "cook", "cool", "copy", "corn", "corner", "correct", "cost", "cotton", "could", "couldn't", "count", "country", "course", "covered", "cows", "create", "cried", "crops", "cross", "crowd", "current", "cut","dance", "dark", "day", "dead", "deal", "death", "decided", "decimal", "deep", "describe", "desert", "design", "details", "determine", "developed", "dictionary", "did", "didn't", "died", "difference", "different", "difficult", "direct", "direction", "discovered", "distance", "divided", "division", "do", "doctor", "does", "doesn't", "dog", "dollars", "done", "don't", "door", "down", "draw", "drawing", "dress", "drive", "drop", "dry", "during","each", "early", "ears", "earth", "east", "easy", "eat", "edge", "effect", "eggs", "eight", "either", "electric", "elements", "else", "end", "energy", "engine", "England", "English", "enjoy", "enough", "entered", "entire", "equal", "equation", "especially", "Europe", "even", "evening", "ever", "every", "everyone", "everything", "exactly", "example", "except", "exciting", "exercise", "expect", "experience", "experiment", "explain", "express", "eye","face", "fact", "factories", "factors", "fall", "family", "famous", "far", "farm", "farmers", "fast", "father", "fear", "feel", "feeling", "feet", "fell", "felt", "few", "field", "fig", "fight", "figure", "filled", "finally", "find", "fine", "fingers", "finished", "fire", "first", "fish", "fit", "five", "flat", "floor", "flow", "flowers", "fly", "follow", "food", "foot", "for", "force", "forest", "form", "forward", "found", "four", "fraction", "France", "free", "French", "fresh", "friends", "from", "front", "fruit", "full", "fun","game", "garden", "gas", "gave", "general", "get", "girl", "give", "glass", "go", "God", "gold", "gone", "good", "got", "government", "grass", "great", "Greek", "green", "grew", "ground", "group", "grow", "guess", "gun","had", "hair", "halt", "hand", "happened", "happy", "hard", "has", "hat", "have", "he", "head", "hear", "heard", "heart", "heat", "heavy", "held", "help", "her", "here", "high", "hill", "him", "himself", "his", "history", "hit", "hold", "hole", "home", "hope", "horse", "hot", "hours", "house", "how", "however", "huge", "human", "hundred", "hunting","I", "ice", "idea", "if", "I'll", "important", "in", "inches", "include", "increase", "Indian", "indicate", "industry", "information", "insects", "inside", "instead", "instruments", "interest", "interest", "into", "iron", "is", "island", "isn't", "it", "its", "it's", "itself","Japanese", "job", "joined", "jumped", "just","keep", "kept", "key", "killed", "kind", "king", "knew", "know", "known","lady", "lake", "land", "language", "large", "last", "later", "laughed", "law", "lay", "lead", "learn", "least", "leave", "led", "left", "legs", "length", "less", "let", "let's", "letter", "level", "lie", "life", "lifted", "light", "like", "line", "list", "listen", "little", "live", "located", "long", "look", "lost", "lot", "loud", "love", "low","machine", "made", "main", "major", "make", "man", "many", "map", "march", "mark", "match", "material", "matter", "may", "maybe", "me", "mean", "measure", "meat", "meet", "melody", "members", "men", "metal", "method", "middle", "might", "mile", "milk", "million", "mind", "mine", "minutes", "miss", "modern", "molecules", "moment", "money", "months", "moon", "more", "morning", "most", "mother", "mountain", "mouth", "move", "movement", "much", "music", "must", "my","name", "nation", "natural", "near", "necessary", "need", "never", "new", "next", "night", "no", "nor", "north", "northern", "nose", "not", "note", "nothing", "notice", "noun", "now", "number", "numeral","object", "observe", "ocean", "of", "off", "office", "often", "oh", "oil", "old", "on", "once", "one", "only", "open", "opposite", "or", "order", "other", "our", "out", "outside", "over", "own", "oxygen","page", "paint", "pair", "paper", "paragraph", "park", "part", "particular", "party", "passed", "past", "pattern", "pay", "people", "per", "perhaps", "period", "person", "phrase", "picked", "picture", "piece", "place", "plains", "plan", "plane", "plant", "plants", "play", "please", "plural", "poem", "point", "pole", "poor", "position", "possible", "pounds", "power", "practice", "prepared", "presidents", "pretty", "printed", "probably", "problem", "process", "produce", "products", "property", "provide", "pulled", "pushed", "put","questions", "quickly", "quiet", "quite","race", "radio", "rain", "raised", "ran", "rather", "reached", "read", "ready", "really", "reason", "received", "record", "red", "region", "remain", "remember", "repeated", "report", "represent", "resent", "rest", "result", "return", "rhythm", "rich", "ride", "right", "ring", "rise", "river", "road", "rock", "rolled", "room", "root", "rope", "rose", "round", "row", "rule", "run","safe", "said", "sail", "same", "sand", "sat", "save", "saw", "say", "scale", "school", "science", "scientists", "score", "sea", "seat", "second", "section", "see", "seeds", "seem", "seen", "sell", "send", "sense", "sent", "sentence", "separate", "serve", "set", "settled", "seven", "several", "shall", "shape", "sharp", "she", "ship", "shoes", "shop", "short", "should", "shoulder", "shouted", "show", "shown", "side", "sight", "sign", "signal", "silent", "similar", "simple", "since", "sing", "sir", "sister", "sit", "six", "size", "skin", "sky", "sleep", "sleep", "slowly", "small", "smell", "smiled", "snow", "so", "soft", "soil", "soldiers", "solution", "some", "someone", "something", "sometimes", "son", "song", "soon", "sound", "south", "southern", "space", "speak", "special", "speed", "spell", "spot", "spread", "spring", "square", "stand", "stars", "start", "state", "statement", "stay", "steel", "step", "stick", "still", "stone", "stood", "stop", "store", "story", "straight", "strange", "stream", "street", "stretched", "string", "strong", "students", "study", "subject", "substances", "such", "suddenly", "suffix", "sugar", "suggested", "sum", "summer", "sun", "supply", "suppose", "sure", "surface", "surprise", "swim", "syllables", "symbols", "system","table", "tail", "take", "talk", "tall", "teacher", "team", "tell", "temperature", "ten", "terms", "test", "than", "that", "the", "their", "them", "themselves", "then", "there", "these", "they", "thick", "thin", "thing", "think", "third", "this", "those", "though", "thought", "thousands", "three", "through", "thus", "tied", "time", "tiny", "to", "today", "together", "told", "tone", "too", "took", "tools", "top", "total", "touch", "toward", "town", "track", "trade", "train", "train", "travel", "tree", "triangle", "trip", "trouble", "truck", "true", "try", "tube", "turn", "two", "type","uncle", "under", "underline", "understand", "unit", "until", "up", "upon", "us", "use", "usually","valley", "value", "various", "verb", "very", "view", "village", "visit", "voice", "vowel","wait", "walk", "wall", "want", "war", "warm", "was", "wash", "Washington", "wasn't", "watch", "water", "waves", "way", "we", "wear", "weather", "week", "weight", "well", "we'll", "went", "were", "west", "western", "what", "wheels", "when", "where", "whether", "which", "while", "white", "who", "whole", "whose", "why", "wide", "wife", "wild", "will", "win", "wind", "window", "wings", "winter", "wire", "wish", "with", "within", "without", "woman", "women", "wonder", "won't", "wood", "word", "work", "workers", "world", "would", "wouldn't", "write", "written", "wrong", "wrote","yard", "year", "yellow", "yes", "yet", "you", "young", "your", "you're", "yourself"];



describe("Highlighting", function() {
    var snowball = Snowball('English');
    var stemmedCommonWords = [];
    for(var termIdx = 0; termIdx < commonWords.length; termIdx++) {
        snowball.setCurrent(commonWords[termIdx].toLowerCase());
        snowball.stem();
        stemmedCommonWords.push(snowball.getCurrent());
    }
    function checkWord(word) {
        var wordIdx,
            stemmedTerm;

        snowball.setCurrent(word.toLowerCase());
        snowball.stem();
        stemmedTerm = snowball.getCurrent();
        for(wordIdx = 0; wordIdx < stemmedCommonWords.length; wordIdx++) {

            if(stemmedTerm === stemmedCommonWords[wordIdx])
                return true;

        }

        return stemmedTerm.search(/\W/) !== -1;
    }

    function insertContent() {
        var popups = document.getElementsByClassName('popup');
        for( var i = 0; i < popups.length; i++) {
            document.body.removeChild(popups[i]);
        }
        document.getElementById('text_wrapper').innerHTML =
        "<p>Mrs. Dalloway said she would buy the flowers herself.</p>\
        <p>\
        For Lucy had her work cut out for her.  The doors would be taken\
        off their hinges; Rumpelmayer's men were coming.  And then, thought\
        Clarissa Dalloway, what a morning--fresh as if issued to children\
        on a beach.\
        </p>\
        <p>\
        What a lark!  What a plunge!  For so it had always seemed to her,\
        when, with a little squeak of the hinges, which she could hear now,\
        she had burst open the French windows and plunged at Bourton into\
        the open air.  How fresh, how calm, stiller than this of course,\
        the air was in the early morning; like the flap of a wave; the kiss\
        of a wave; chill and sharp and yet (for a girl of eighteen as she\
        then was) solemn, feeling as she did, standing there at the open\
        window, that something awful was about to happen; looking at the\
        flowers, at the trees with the smoke winding off them and the rooks\
        rising, falling; standing and looking until Peter Walsh said,\
        \"Musing among the vegetables?\"--was that it?--\"I prefer men to\
        cauliflowers\"--was that it?  He must have said it at breakfast one\
        morning when she had gone out on to the terrace--Peter Walsh.  He\
        would be back from India one of these days, June or July, she\
        forgot which, for his letters were awfully dull; it was his sayings\
        one remembered; his eyes, his pocket-knife, his smile, his\
        grumpiness and, when millions of things had utterly vanished--how\
        strange it was!--a few sayings like this about cabbages.\
        </p>\
        <p>\
        She stiffened a little on the kerb, waiting for Durtnall's van to\
        pass.  A charming woman, Scrope Purvis thought her (knowing her as\
        one does know people who live next door to one in Westminster); a\
        touch of the bird about her, of the jay, blue-green, light,\
        vivacious, though she was over fifty, and grown very white since\
        her illness.  There she perched, never seeing him, waiting to\
        cross, very upright.\
        </p>\
        <p>\
        For having lived in Westminster--how many years now? over twenty,--\
        one feels even in the midst of the traffic, or waking at night,\
        Clarissa was positive, a particular hush, or solemnity; an\
        indescribable pause; a suspense (but that might be her heart,\
        affected, they said, by influenza) before Big Ben strikes.  There!\
        Out it boomed.  First a warning, musical; then the hour,\
        irrevocable.  The leaden circles dissolved in the air.  Such fools\
        we are, she thought, crossing Victoria Street.  For Heaven only\
        knows why one loves it so, how one sees it so, making it up,\
        building it round one, tumbling it, creating it every moment\
        afresh; but the veriest frumps, the most dejected of miseries\
        sitting on doorsteps (drink their downfall) do the same; can't be\
        dealt with, she felt positive, by Acts of Parliament for that very\
        reason: they love life.  In people's eyes, in the swing, tramp, and\
        trudge; in the bellow and the uproar; the carriages, motor cars,\
        omnibuses, vans, sandwich men shuffling and swinging; brass bands;\
        barrel organs; in the triumph and the jingle and the strange high\
        singing of some aeroplane overhead was what she loved; life;\
        London; this moment of June.\
        </p>\
        <p>brothers-in-law, brother-in-law's, brothers-in-law's  <span>brother-in-law</span></p>\
        ";
    }

    beforeEach(function() {
        document.body.className="";
        insertContent();

        addHighlighting();
    });

    it("should not highlight any single word terms not on the list", function() {
        this.addMatchers({
            toBeInWordList: function() {

                this.message = function() {
                    return "Expected '" + this.actual + "' to be in the word list";
                }
                return checkWord(this.actual);
            }
        })
        waitsFor(function() {
            return document.body.className.search("highlighted_text") !== -1;
        });
        runs(function() {
            var highlightedSpans = document.getElementsByClassName('glossary_term'),
                spanIdx;
            for (spanIdx = 0; spanIdx < highlightedSpans.length; spanIdx++) {

                expect(highlightedSpans.item(spanIdx).textContent).toBeInWordList();
            }
        })


  });

    it("should highlight at least 100 words", function() {
        waitsFor(function() {
            return document.body.className.search("highlighted_text") !== -1;
        });
        runs(function() {
            expect(document.getElementsByClassName('glossary_term').length).toBeGreaterThan(100);

        })
    });

    it("should not alter the text while highlighting", function() {
        var oldText = document.getElementById('text_wrapper').textContent;
        waitsFor(function() {
            return document.body.className.search("highlighted_text") !== -1;
        });
        runs(function() {
            var tCont = document.getElementById('text_wrapper').textContent;
            for(var textIdx = 0; textIdx < oldText.length; textIdx++) {
                if(tCont[textIdx] !== oldText[textIdx]) {
                    console.log("Old: " + oldText.substr(textIdx - 5, 10) + ", New: " + tCont.substr(textIdx - 5, 10));
                    break;
                }
            }
            expect(document.getElementById('text_wrapper').textContent).toBe(oldText);
        })
    });

    it("should contain the highlighting for tricky words", function () {
        this.addMatchers({
            toBeInSet: function(expectedSet) {
                this.message = function() {
                    return "Expected to find '" + this.actual + "' in found word list"
                }
                return expectedSet[this.actual] !== undefined;

            }
        });
        waitsFor(function() {
            return document.body.className.search("highlighted_text") !== -1;
        });
        runs(function() {
            var highlightedSpans = document.getElementsByClassName('glossary_term'),
                spanIdx,
                foundWords = {};
            for (spanIdx = 0; spanIdx < highlightedSpans.length; spanIdx++) {
                foundWords[highlightedSpans[spanIdx].textContent] = true;
            }

            expect("Mrs. Dalloway").toBeInSet(foundWords);
            expect("herself.").toBeInSet(foundWords);
            expect("For Lucy").toBeInSet(foundWords);
            expect("very upright").toBeInSet(foundWords);
            expect("brothers-in-law").toBeInSet(foundWords);
            expect("brother-in-law").toBeInSet(foundWords);

        })
    });

    it("should allow clicking on highlighted words", function () {
      var done = false;
        waitsFor(function() {
            return document.body.className.search("highlighted_text") !== -1;
        });
        runs(function() {
            var highlightedSpans = document.getElementsByClassName('glossary_term'),
                spanIdx;

            for (spanIdx = 0; spanIdx < highlightedSpans.length; spanIdx++) {
                if(highlightedSpans[spanIdx].textContent === 'the') {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window,
                        0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    highlightedSpans[spanIdx].dispatchEvent(evt);
                    break;
                }
            }
            expect(document.getElementsByClassName('popup')[0].style.display).toBe('block');

        });


    });
});